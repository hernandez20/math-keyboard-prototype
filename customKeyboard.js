
let mykeyList=[
    `<math xmlns='http://www.w3.org/1998/Math/MathML'>
    <mrow>
     <mi>cos</mi>
     <mo>&#8289;</mo>
     <mo>(</mo>
     <msup>
      <mi>x</mi>
      <mn>3</mn>
     </msup>
     <mo>)</mo>
    </mrow>
   </math>`,
    "B",
    "C",
    "D",
    "E",
    "F",
    "G"
]

// Uso de los Builders
const keyboard = new KeyboardBuilder()
  .setWidth(window.innerWidth + "px")
  .setColor('dark')
  .build();

const keyList = new KeyListBuilder().addKeyList(mykeyList).build(keyboard);

//   .addKey("B")
//   .addKey("C")
//   .addKey("D")
//   .addKey(    `<math xmlns='http://www.w3.org/1998/Math/MathML'>
//   <mrow>
//    <mi>cos</mi>
//    <mo>&#8289;</mo>
//    <mo>(</mo>
//    <msup>
//     <mi>x</mi>
//     <mn>3</mn>
//    </msup>
//    <mo>)</mo>
//   </mrow>
//  </math>`,)







