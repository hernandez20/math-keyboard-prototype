class Keyboard {
  // Clase para representar el teclado
  constructor(width, keys,color) {
    this.width = width;
    this.keys = keys;
    this.color = color;
  }
}
class KeyboardBuilder {
  // Clase para construir el teclado
  constructor() {
    this.keyboard = new Keyboard();
  }

  setWidth(width) {
    this.keyboard.width = width;
    return this;
  }

  setColor(color){
    this.keyboard.color = color
    return this;

  }

  build() {
    const keyboardElement = document.createElement("div");
    keyboardElement.classList.add("keyboard");
    keyboardElement.classList.add(`profile--${this.keyboard.color}`)
    keyboardElement.style.width = this.keyboard.width;
    return keyboardElement;
  }
}
class KeyListBuilder {
  // Clase para construir la lista de teclas
  constructor() {
    this.keyList = [];
    
  }

  addKey(key) {
    this.keyList.push(key);
    return this;
  }
  addKeyList(key){
    
    key.forEach((keyValue) => {
      console.log(keyValue)

      this.keyList.push(keyValue);
     // console.log(keyList)

    })
    return this;

  }


  build(keyboard) {
    this.keyList.forEach((keyValue) => {
     let key =document.createElement("div")
      key.classList.add("keyboard__key", "keyboard__key--special", "col-span-2");
      key.setAttribute( "id","Escape")
      key.setAttribute("value", "a")
      key.insertAdjacentHTML("beforeend",keyValue)
      keyboard.appendChild(key);
      document.body.appendChild(keyboard)

    });

    return this.keyList;
  }
}









