const keyboard = document.createElement("div");
keyboard.classList.add("keyboard");
keyboard.style.width = window.innerWidth + "px";
// window.addEventListener("resize", () => {
//     div.style.width = window.innerWidth + "px";
//   });
document.body.appendChild(keyboard)


keyList=[
    `<math xmlns='http://www.w3.org/1998/Math/MathML'>
    <mrow>
     <mi>cos</mi>
     <mo>&#8289;</mo>
     <mo>(</mo>
     <msup>
      <mi>x</mi>
      <mn>3</mn>
     </msup>
     <mo>)</mo>
    </mrow>
   </math>`,
    "B",
    "C",
    "D",
    "E",
    "F",
    "G"
]
generateKey(keyList)

function generateKey(keyList){
    keyList.forEach(keyValue => {
        key =document.createElement("div")
        key.classList.add("keyboard__key", "keyboard__key--special", "col-span-2");
        key.setAttribute( "id","Escape")
        key.setAttribute("value", "a")
        key.insertAdjacentHTML("beforeend",keyValue)
        keyboard.appendChild(key)
    });
}
function renderLaTeX(latex) {
  return MathJax.tex2chtml(latex, { display: true });
}

const latexInput = "\\sqrt{\\left(\\right)}";
const renderedMath = renderLaTeX(latexInput);
const divContainer = document.createElement("div");


let focused
let inputs = document.querySelectorAll('#div-input')

document.querySelector('#div-input').addEventListener('click', function(e) {
  const clickedElement = e.target.closest('mjx-box');

  const insertionPoint = e.target.closest('mjx-box');
  const index = Array.from(this.querySelectorAll('mjx-box')).indexOf(clickedElement);
console.log(index)
})
//divContainer.innerHTML = renderedMath.outerHTML;
document.body.appendChild(divContainer);

inputs.forEach(element => {
    element.addEventListener("click", () => {
        focused= element
    
      });
});

  document.querySelectorAll(".keyboard .keyboard__key").forEach((button) => {
    button.addEventListener("click", (e) => {
      console.log(button.textContent)
       // focused.textContent += button.textContent;
       const spanContainer = document.createElement("span");
       focused.innerHTML= renderedMath.outerHTML

    });
  });


// Obtener la expresión matemática en formato LaTeX
